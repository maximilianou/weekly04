CREATE DATABASE votesdb;

USE votesdb;

CREATE TABLE users (
  usr_id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE,
  email VARCHAR(255),
  date_created TIMESTAMP
);

CREATE TABLE nominations (
  nom_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  nom_usr_id INT REFERENCES users(usr_id),
  date_created TIMESTAMP
);

CREATE TABLE nom_vote_items (
  nom_it_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  nom_id INT REFERENCES nominations(nom_id),
  nom_usr_id INT REFERENCES users(usr_id),
  nom_vote_usr_id INT REFERENCES users(usr_id),
  date_created TIMESTAMP
);
