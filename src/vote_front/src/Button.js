// Button.js
import React, { useState } from 'react';

const Button = () => {
    const [btnTxt, setBtnTxt] = useState("Click This Button, Please!");
    const handleClick = () => {
        return setBtnTxt(`Thanks!! ${(new Date()).toISOString()}!! `);
    }
    return <button onClick={handleClick}>{btnTxt}</button>;
};

export default Button;