// App.js
import React from 'react';
import './App.css';
import Button from './Button';
import TheRef from './TheRef';
import AndContextual from './AndContextual';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
      </header>
      <Button />
      <TheRef />
      <AndContextual />
    </div>
  );
}

export default App;
