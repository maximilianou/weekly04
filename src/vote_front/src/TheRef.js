// TheRef.js
import React, { useState, useRef } from 'react';

const TheRef = () => {
    const [theTitle, setTheTitle] = useState("The Game is Now");
    const titleRef = useRef();
    const changeTitle = () => {
        setTheTitle(titleRef.current.value);
    };
    return (
        <article>
            <header>
                <h3>{theTitle}</h3>
            </header>
            <div>
                <input ref={titleRef} type='text' />
                <button onClick={changeTitle}>
                    Change
                </button>
            </div>
        </article>
    );
};

export default TheRef;