// AndContextual.js
import React, { useContext, createContext } from 'react';

const SomeContext = createContext();

const Show = () => {
    const theValue = useContext(SomeContext);
    return <div>{theValue}, This came from outside Context</div>;
};

const AndContextual = () => {
    return (
        <SomeContext.Provider value={'Outside Values'}>
            <Show />
        </SomeContext.Provider>
    );
};

export default AndContextual;