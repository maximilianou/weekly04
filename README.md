# weekly04

learning in public

https://weeklyproject.club/

This week you'll be working with Ann-Marie, the president of her local honor society.  
Her honor society meets every week in groups of about 30 people, led by a group leader.  
The honor society has two kinds of members, regular members that everybody starts as, and exceptional members.  

In order to become an exceptional member someone has to

 1. Be a member for at least 2 years
 2. Do 100 hours of community service
 3. Be nominated by their group leader
 4. Be elected by at least 50% of the group.

Currently, Ann-Marie goes to each group and runs an election, doing the whole process manually.  Each member gets a ballot with the list of nominees, and they mark those ballots with who they want to become an exceptiona memberl. 
They can mark more than one if they so choose. 
If 5 people are nominated all 5 of them can become exceptionals.

Here’s an example of how an election might go:

Dani, Sarah, and Jimmy all meet criteria 1 and 2.  
Their group leader nominates them.  
On the day of the election, 18 people show up to the group meeting.  
The group votes and Sarah gets 12 votes, Dani gets 9 and Jimmy gets 8.  
Sarah and Dani are now exceptional members, and Jimmy has to try again next year.

The honor society is getting too large for Ann-Marie to run all of these elections by herself, so this week build something to help her out!  Some useful things would be:

 1. Easy vote tallying
 2. Keeping track of new exceptional members in a text file
 3. Printable ballot generation
 4. Something online that she can send to group leaders.

-----

Board

https://gitlab.com/maximilianou/weekly04/-/boards/1577807

-----

.gitignore

https://github.com/github/gitignore/blob/master/Node.gitignore

```
mkdir src
cd src
touch docker-compose.yml
```

```
version: '3.7'
services:
  vote_front: 
    image: node:alpine
    volumes:
      - ./vote_front/.:/srv
    working_dir: /srv
    command: npm run start
    ports: 
      - 2120:3000
    environment:
      - NODE_ENV=development
    networks:
      - net_intranet

  vote_back: 
    image: node:alpine
    volumes:
      - ./vote_back/.:/srv
    working_dir: /srv
    command: npm run start
    ports: 
      - 2140:3000      
    environment:
      - NODE_ENV=development
    networks:
      - net_intranet

  db:
    image: postgres:alpine
    environment:
      POSTGRES_PASSWORD: example
    ports: 
      - 2160:5432
    networks:
      - net_intranet
        
  adminer:
    image: adminer
    ports:
      - 2150:8080
    networks:
      - net_intranet
    
networks:
  net_intranet: {}
```

```
npx create-react-app vote_front
express vote_back
cd vote_front
npm install
cd ../vote_back
npm install
cd ..
docker-compose up
```

```
CREATE DATABASE votesdb;

USE votesdb;

CREATE TABLE users (
  usr_id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE,
  email VARCHAR(255),
  date_created TIMESTAMP
);

CREATE TABLE nominations (
  nom_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  nom_usr_id INT REFERENCES users(usr_id),
  date_created TIMESTAMP
);

CREATE TABLE nom_vote_items (
  nom_it_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  nom_id INT REFERENCES nominations(nom_id),
  nom_usr_id INT REFERENCES users(usr_id),
  nom_vote_usr_id INT REFERENCES users(usr_id),
  date_created TIMESTAMP
);

```


----


```
// App.js
import React from 'react';
import './App.css';
import Button from './Button';
import TheRef from './TheRef';
import AndContextual from './AndContextual';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
      </header>
      <Button />
      <TheRef />
      <AndContextual />
    </div>
  );
}
export default App;
```

```
// Button.js
import React, { useState } from 'react';
const Button = () => {
    const [btnTxt, setBtnTxt] = useState("Click This Button, Please!");
    const handleClick = () => {
        return setBtnTxt(`Thanks!! ${(new Date()).toISOString()}!! `);
    }
    return <button onClick={handleClick}>{btnTxt}</button>;
};
export default Button;
```

```
// TheRef.js
import React, { useState, useRef } from 'react';
const TheRef = () => {
    const [theTitle, setTheTitle] = useState("The Game is Now");
    const titleRef = useRef();
    const changeTitle = () => {
        setTheTitle(titleRef.current.value);
    };
    return (
        <article>
            <header>
                <h3>{theTitle}</h3>
            </header>
            <div>
                <input ref={titleRef} type='text' />
                <button onClick={changeTitle}>
                    Change
                </button>
            </div>
        </article>
    );
};
export default TheRef;
```

```
// AndContextual.js
import React, { useContext, createContext } from 'react';
const SomeContext = createContext();
const Show = () => {
    const theValue = useContext(SomeContext);
    return <div>{theValue}, This came from outside Context</div>;
};
const AndContextual = () => {
    return (
        <SomeContext.Provider value={'Outside Values'}>
            <Show />
        </SomeContext.Provider>
    );
};
export default AndContextual;
```

----
